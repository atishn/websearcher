package com.wework.validator;

import org.jsoup.helper.StringUtil;
import org.springframework.stereotype.Component;

/**
 * The Input file validator.
 */
@Component
public class InputFileValidator {

    /**
     * Validate header line. Quicky solution.
     *
     * @param headerString the header
     *
     * @return the boolean
     */
    public boolean validateHeader(String headerString) {
        if (headerString == null || headerString.isEmpty()) return false;
        String[] headers = headerString.split(",");
        if (headers.length != 6) return false;
        // ToDo more validations
        return true;
    }

    /**
     * Validate site url. Quicky solution.
     *
     * @param urlString the url
     *
     * @return the boolean
     */
    public boolean validateSiteUrl(String urlString) {
        if (urlString == null || urlString.isEmpty()) return false;
        String[] headers = urlString.split(",");
        if (headers.length != 6) return false;
        if (!StringUtil.isNumeric(headers[0])) return false;   // Rank
        if (headers[1] == null || headers[1].isEmpty()) return false; // URL
        // ToDo more validations
        return true;
    }

}
