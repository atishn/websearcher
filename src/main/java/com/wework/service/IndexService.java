package com.wework.service;

import com.wework.model.SiteUrl;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * The Index service.
 */
@Service
public class IndexService {

    /**
     * The Class name.
     */
    private final String className = IndexService.class.getName();
    /**
     * The Index map.
     */
    protected Map<String, List<SiteUrl>> indexMap;
    /**
     * The Site url list.
     */
    private List<SiteUrl> siteUrlList;

    /**
     * Build index. Uses JSoup library to extract text from HTML String content and build Search Index
     * data structure.
     * <p>
     * It identies all Unique Search term from the downloaded content and builds the index Map with Search term as a key
     * and all the documents in which search term found as a value.
     * <p>
     * Map<String searchTerm, List<SiteUrl>results>
     *
     * @param urls the site url list
     *
     * @return the map
     */
    public void buildSearchIndex(final List<SiteUrl> urls) throws Exception {
        if (urls == null || urls.isEmpty()) throw new Exception("Empty input URL file");
        this.siteUrlList = urls;

        Map<String, List<SiteUrl>> searchTermMap = new ConcurrentHashMap();
        for (SiteUrl siteUrl : siteUrlList) {
            String content = siteUrl.getContent();
            if (content != null && !content.isEmpty()) {
                String lowerCaseText = Jsoup.parse(content).text().toLowerCase();
                String[] unique = Arrays.stream(lowerCaseText.split("\\s")).distinct().toArray(String[]::new);
                for (String searchTerm : unique) {
                    searchTermMap.compute(searchTerm, (key, value) -> {
                        if (value == null) {
                            value = new LinkedList();
                        }
                        value.add(siteUrl);
                        return value;
                    });
                }
            }
        }
        indexMap = searchTermMap;
    }


    /**
     * Search the index for given search term.
     * <p>
     * It first look for search term in the prebuilt indexMap, if found returns the List of Urls.
     * If not found, it falls back to regex and look for the search term in the downloaded content.
     *
     * @param searchTerm the search term
     *
     * @return the list
     *
     * @throws Exception the exception
     */
    public List<SiteUrl> search(String searchTerm) throws Exception {

        if (searchTerm == null || searchTerm.isEmpty()) return null;

        if (indexMap == null || indexMap.isEmpty()) {
            Logger.getLogger(className).log(Level.SEVERE, "Web Pages has not been loaded yet. Check the input URL text file!");
            throw new Exception("Error in input url text file");
        }

        String[] terms = searchTerm.toLowerCase().split("\\s");

        List<SiteUrl> result = new ArrayList();
        for (String term : terms) {
            if (indexMap.containsKey(term))
                result.addAll(indexMap.get(term));
            else {
                List<SiteUrl> regexResult = siteUrlList.stream().filter(url ->
                        url.getContent() != null && Pattern.compile(term).matcher(url.getContent()).find()).
                        collect(Collectors.toList());
                result.addAll(regexResult);
                indexMap.put(term, result); // Caching for future use.
            }
        }

        if (terms.length > 1) {
            // Remove duplicates and sort the result
            return result.stream()
                    .distinct()
                    .sorted(Comparator.comparing(SiteUrl::getRank))
                    .collect(Collectors.toList());
        } else
            return result;
    }

}
