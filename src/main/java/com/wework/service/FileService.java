package com.wework.service;

import com.wework.model.SiteUrl;
import com.wework.validator.InputFileValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * The File service. Manages input and output file operations.
 */
@Service
public class FileService {

    /**
     * The Class name.
     */
    private final String className = FileService.class.getName();
    /**
     * The Input file validator.
     */
    @Autowired
    private InputFileValidator inputFileValidator;

    /**
     * Load Incoming URL file.
     *
     * @param inputFile the input file
     *
     * @return the list
     *
     * @throws Exception the exception
     */
    public List<SiteUrl> loadUrlFile(String inputFile) throws Exception {

        Path urlPath = Paths.get(inputFile);
        if (urlPath == null) throw new Exception("Input file does not exist" + inputFile);
        Optional<String> headerLine = Files.lines(urlPath).findFirst();
        String headers = headerLine.get();

        List<SiteUrl> siteUrlList = new ArrayList();

        if (inputFileValidator.validateHeader(headers)) {
            Files.lines(urlPath).skip(1).forEach(line -> {
                if (inputFileValidator.validateSiteUrl(line)) {
                    String[] urlLine = line.split(",");
                    int rank = Integer.valueOf(urlLine[0]);
                    String url = urlLine[1].replaceAll("\"", "");
                    SiteUrl siteUrl = new SiteUrl(rank, url);
                    siteUrlList.add(siteUrl);
                }
            });
        }

        if (siteUrlList.isEmpty()) {
            Logger.getLogger(className).log(Level.SEVERE, "No valid SiteUrls found in input txt file.");
            throw new Exception("Error in input url text file");
        }
        return siteUrlList;
    }

    /**
     * Update results to output file. If output file does not exist, it will create it.
     *
     * @param fileName   the file name
     * @param searchTerm the search term
     * @param results    the results
     */
    public void outputResult(String fileName, String searchTerm, List<SiteUrl> results) throws Exception {

        Path outputFile = Paths.get(fileName);
        List<String> output = new ArrayList<>();
        output.add("Search Term - " + searchTerm);
        if (results == null || results.isEmpty()) {
            output.add("No results found!");
        } else {
            output.add("Given search Term found on " + results.size() + " pages.");
            output.addAll(results.stream().map(SiteUrl::getUrl).collect(Collectors.toList()));
        }

        output.add("--------------------------------------");
        output.add("--------------------------------------");
        output.add("--------------------------------------");

        try {
            Files.write(outputFile, output, StandardCharsets.UTF_8, Files.exists(outputFile) ? StandardOpenOption.APPEND : StandardOpenOption.CREATE);
        } catch (IOException ex) {
            Logger.getLogger(className).log(Level.SEVERE, "Web Pages has not been loaded yet!");

        }
    }
}
