package com.wework.model;

import java.util.Objects;

/**
 * The Site url Model.
 */
public class SiteUrl {
    /**
     * The Rank.
     */
    int rank;
    /**
     * The Url.
     */
    String url;
    /**
     * The Content.
     */
    String content;

    /**
     * Instantiates a new Site url.
     *
     * @param rank the rank
     * @param url the url
     */
    public SiteUrl(int rank, String url) {
        this.rank = rank;
        this.url = url;
    }


    /**
     * Equals boolean.
     *
     * @param obj the obj
     * @return the boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof SiteUrl)) {
            return false;
        }
        SiteUrl site = (SiteUrl) obj;
        return rank == site.rank &&
                Objects.equals(url, site.url);
    }

    /**
     * Gets rank.
     *
     * @return the rank
     */
    public int getRank() {
        return rank;
    }

    /**
     * Sets rank.
     *
     * @param rank the rank
     */
    public void setRank(int rank) {
        this.rank = rank;
    }

    /**
     * Gets url.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets url.
     *
     * @param url the url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Gets content.
     *
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets content.
     *
     * @param content the content
     */
    public void setContent(String content) {
        this.content = content;
    }

}
