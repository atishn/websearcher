package com.wework.util;

import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The Page downloader Component.
 */
@Component
public class PageDownloader {
    /**
     * The constant className.
     */
    final static String className = PageDownloader.class.getName();
    /**
     * The User agent.
     */
    final String userAgent = "Mozilla/5.0";
    /**
     * The Max connections.
     */
    @Value("${maxConnections}")
    private int maxConnections;
    /**
     * The Timeout.
     */
    @Value("${httpTimeout}")
    private int timeout;

    /**
     * Load site content.
     *
     * @param urls the urls
     *
     * @return the map
     *
     * @throws InterruptedException the interrupted exception
     */
    public Map<String, String> loadSiteContent(final List<String> urls) throws InterruptedException {
        return loadSiteContent(urls, maxConnections);
    }

    /**
     * Load site content.
     *
     * @param urls           the urls
     * @param maxConnections the max http connections
     *
     * @return the map
     *
     * @throws InterruptedException the interrupted exception
     */
    public Map<String, String> loadSiteContent(final List<String> urls, int maxConnections) throws InterruptedException {
        if (urls == null || urls.isEmpty()) return null;
        final Semaphore semaphore = new Semaphore(maxConnections);

        AtomicInteger success = new AtomicInteger(0);
        AtomicInteger failed = new AtomicInteger(0);

        Map<String, String> contentMap = new ConcurrentHashMap();
        for (String url : urls) {
            try {
                semaphore.acquire();
                new Thread(() -> {
                    String location = url;
                    if (!location.toLowerCase().matches("^\\w+://.*")) {
                        location = "http://" + location;
                    }
                    try {
                        String content = Jsoup.connect(location)
                                .userAgent(userAgent).timeout(timeout)
                                .get().html();
                        contentMap.put(url, content);
                        success.incrementAndGet();
                        Logger.getLogger(className).log(Level.INFO, "Page Loaded " + location);
                    } catch (SocketTimeoutException ex) {
                        Logger.getLogger(className).log(Level.SEVERE, ex.toString() + " " + location);
                        failed.incrementAndGet();
                    } catch (IOException ex) {
                        Logger.getLogger(className).log(Level.SEVERE, ex.toString());
                        failed.incrementAndGet();
                    } finally {
                        semaphore.release();
                    }
                }).start();

            } catch (InterruptedException ex) {
                semaphore.release();
            }
        }

        while (semaphore.availablePermits() < maxConnections) { //if still have running child thread, wait for a sec.
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                Logger.getLogger(className).log(Level.SEVERE, "Main Thread Interrupted.", e);
                throw e;
            }
        }

        Logger.getLogger(className).log(Level.INFO, "Number of Page Loaded " + success.get());
        Logger.getLogger(className).log(Level.INFO, "Number of Page failed to Load " + failed.get());

        return contentMap;
    }
}
