package com.wework;

import com.wework.model.SiteUrl;
import com.wework.service.FileService;
import com.wework.service.IndexService;
import com.wework.util.PageDownloader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

import static java.lang.System.exit;

/**
 * The Web searcher.
 */
@SpringBootApplication
@Profile("!test")
public class WebSearcher implements CommandLineRunner {

    /**
     * The File service.
     */
    @Autowired
    private FileService fileService;

    /**
     * The Index service.
     */
    @Autowired
    private IndexService indexService;

    /**
     * The Page downloader.
     */
    @Autowired
    private PageDownloader pageDownloader;

    /**
     * The Max connections.
     */
    @Value("${inputFile}")
    private String inputFile;
    /**
     * The Timeout.
     */
    @Value("${outputFile}")
    private String outputFile;


    /**
     * The entry point of application.
     *
     * @param args the input arguments
     *
     * @throws Exception the exception
     */
    public static void main(String[] args) throws Exception {

        SpringApplication app = new SpringApplication(WebSearcher.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);

    }

    /**
     * Executes the main program.
     * Three main objectives
     * 1. Load the input url text file.
     * 2. Download the Content and Build Search Index (One time process)
     * 3. Start accepting search term from the user and save the results to output file.
     *
     * @param args the args
     *
     * @throws Exception the exception
     */
    @Override
    public void run(String... args) throws Exception {
        System.out.println("Hi! Welcome to Web Searcher.");

        for (int i = 0; i < args.length; i++) {
            if (args[i].equalsIgnoreCase("-i") && i + 1 < args.length) {
                inputFile = args[i + 1];
            } else if (args[i].equalsIgnoreCase("-o") && i + 1 < args.length) {
                outputFile = args[i + 1];
            }
        }

        System.out.println("All Good. Let me first build the index from the given URL file - " + inputFile);

        // Read the Input Urls.txt file
        List<SiteUrl> siteUrlList = fileService.loadUrlFile(inputFile);

        // Load the Web Pages.
        List<String> urls = siteUrlList.stream().map(SiteUrl::getUrl).collect(Collectors.toList());
        Map<String, String> urlContentMap = pageDownloader.loadSiteContent(urls);
        siteUrlList.forEach(x -> x.setContent(urlContentMap.get(x.getUrl())));


        // Build the Search Index
        indexService.buildSearchIndex(siteUrlList);

        System.out.println("Search Indexes built.");
        System.out.println("--------------------------------------");
        System.out.println("--------------------------------------");
        System.out.println("--------------------------------------");
        System.out.println("Enter search term below- ");

        InputStream source = System.in;
        Scanner in = new Scanner(source);
        while (in.hasNext()) {
            String searchTerms = in.nextLine();
            if (searchTerms != null && !searchTerms.isEmpty()) {
                List<SiteUrl> results = indexService.search(searchTerms);
                if (results != null) {
                    System.out.println("Search Term found on " + results.size() + " pages.");
                    fileService.outputResult(outputFile, searchTerms, results);
                    System.out.println("Results has been dumped to " + outputFile);
                }
                System.out.println("Try new search term!");
            }
        }
        exit(0);
    }
}
