package com.wework.service;

import com.wework.model.SiteUrl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration(classes = {IndexService.class})
public class IndexServiceTest {

    @Autowired
    private IndexService indexService;

    @Before
    public void feedIndex() throws Exception {
        SiteUrl site1 = new SiteUrl(1, "wework.com");
        site1.setContent("Wework - Where Businesses Thrive");

        SiteUrl site2 = new SiteUrl(2, "shopify.com");
        site2.setContent("Shopify - We help Businesses Thrive");

        SiteUrl site3 = new SiteUrl(3, "spacesworks.com");
        site3.setContent("Spacesworks - We offer creative working environments with a unique entrepreneurial spirit");

        List<SiteUrl> urlList = Arrays.asList(site1, site2, site3);
        indexService.buildSearchIndex(urlList);
    }

    @Test
    public void testBuildSearchIndex() throws Exception {

        Map<String, List<SiteUrl>> results = indexService.indexMap;

        assertNotNull(results);
        assertEquals(results.get("businesses").size(), 2);
        assertEquals(results.get("businesses").get(0).getUrl(), "wework.com");
        assertEquals(results.get("businesses").get(1).getUrl(), "shopify.com");

        assertEquals(results.get("spacesworks").size(), 1);
        assertEquals(results.get("spacesworks").get(0).getUrl(), "spacesworks.com");

        // Does not exist
        assertNull(results.get("unknown"));

    }

    @Test
    public void testSearch() throws Exception {

        assertEquals(indexService.search("Businesses").size(), 2);
        assertEquals(indexService.search("Businesses Thrive").size(), 2);
        assertEquals(indexService.search("Shopify Wework SpacesWorks").size(), 3);
    }

    @Test
    public void testSearchWithCaseInsensitive() throws Exception {

        assertEquals(indexService.search("businesses").size(), 2);
        assertEquals(indexService.search("Businesses").size(), 2);
        assertEquals(indexService.search("BUSINESSES").size(), 2);
    }


    @Test
    public void testSearchWithFallBackApproach() throws Exception {
        List<SiteUrl> results = indexService.search("envir");
        assertNotNull(results);
        assertEquals(results.size(), 1);
        assertEquals(results.get(0).getUrl(), "spacesworks.com");

    }


}
