package com.wework.service;

import com.wework.model.SiteUrl;
import com.wework.validator.InputFileValidator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration(classes = {FileService.class, InputFileValidator.class})
public class FileServiceTest {

    @Autowired
    private FileService fileService;


    @Test
    public void testValidLoadUrlFile() throws Exception {
        String fileName = "urls.txt";
        List<SiteUrl> urls = fileService.loadUrlFile(fileName);
        assertTrue(urls != null && !urls.isEmpty());
        assertTrue(urls.size() == 500);

        // Validate Rank exists
        IntStream.rangeClosed(0, 499).forEach(i -> assertTrue(urls.get(i).getRank() == i + 1));

        // Validate Url exists
        IntStream.rangeClosed(0, 499).forEach(i -> assertTrue(urls.get(i).getUrl() != null && !urls.get(i).getUrl().isEmpty()));

    }

    @Test(expected = Exception.class)
    public void testInvalidLoadUrlFile() throws Exception {
        String fileName = "doesnotexist.txt";
        fileService.loadUrlFile(fileName);
    }

    @Test
    public void testOutputResults() throws Exception {
        String fileName = "testresults.txt";
        List<SiteUrl> testResult = new ArrayList<>();
        testResult.add(new SiteUrl(1, "google.com"));
        fileService.outputResult("testResults.txt", "test", testResult);

        Path urlPath = Paths.get(fileName);
        assertNotNull(urlPath);

        List<String> allLines = Files.readAllLines(urlPath);
        assertTrue(allLines != null && !allLines.isEmpty());
        assertEquals(allLines.size(), 6);

        assertTrue(allLines.get(0).equals("Search Term - test"));
        assertTrue(allLines.get(1).equals("Given search Term found on 1 pages."));
        assertTrue(allLines.get(2).equals("google.com"));

        Files.delete(urlPath); // Cleanup
    }


}
