package com.wework.validator;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * The type Input file validator test.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration(classes = {InputFileValidator.class})
public class InputFileValidatorTest {

    /**
     * The Input file validator.
     */
    @Autowired
    private InputFileValidator inputFileValidator;

    /**
     * Test input file header.
     *
     * @throws Exception the exception
     */
    @Test
    public void testInputFileHeader() throws Exception {
        String header = "\"Rank\",\"URL\",\"Linking Root Domains\",\"External Links\",\"mozRank\",\"mozTrust\"\n";

        // Check if total headers are six.
        assertTrue(inputFileValidator.validateHeader(header));

        // Missing few headers.
        header = "\"Rank\", \"Linking Root Domains\",\"External Links\"\n";
        assertFalse(inputFileValidator.validateHeader(header));

    }

    /**
     * Test input file uR ls.
     *
     * @throws Exception the exception
     */
    @Test
    public void testInputFileURLs() throws Exception {
        String urlLine = "1,\"facebook.com/\",9616487,1688316928,9.54,9.34\n";

        // Check if total entries per url lines are six.
        assertTrue(inputFileValidator.validateSiteUrl(urlLine));

        // Missing few entries.
        urlLine = "1xxxxx,\"garbage/\",9.34\n";

        assertFalse(inputFileValidator.validateHeader(urlLine));

    }
}
