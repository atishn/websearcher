package com.wework.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@ContextConfiguration(classes = {PageDownloader.class})
@AutoConfigureMockMvc
public class PageDownloaderTest {

    @Autowired
    private PageDownloader pageDownloader;

    @Test
    public void testLoadSiteContent() throws Exception {
        List<String> urls = Arrays.asList("google.com", "wework.com", "facebook.com");
        Map<String, String> webPageContent = pageDownloader.loadSiteContent(urls);
        assertNotNull(webPageContent);

        assertTrue(webPageContent.containsKey("google.com"));
        assertTrue(webPageContent.containsKey("wework.com"));
        assertTrue(webPageContent.containsKey("facebook.com"));

        assertTrue(webPageContent.get("wework.com").contains("Where Businesses Thrive"));
    }

    @Test
    public void testLoadSiteContentConcurrently() throws Exception {
        List<String> urls = Arrays.asList("google.com", "dell.com", "wework.com", "facebook.com", "adobe.com", "linkedin.com", "amazon.com", "microsoft.com", "vimeo.com", "apple.com", "flickr.com", "sun.com");

        long startTime = System.nanoTime();
        pageDownloader.loadSiteContent(urls, 5);
        long endTime = System.nanoTime();
        long durationWithMultipleConnection = (endTime - startTime);

        startTime = System.nanoTime();
        pageDownloader.loadSiteContent(urls, 1);
        endTime = System.nanoTime();
        long durationWithOneConnection = (endTime - startTime);

        assertTrue(durationWithOneConnection > durationWithMultipleConnection);
    }

    @Test
    public void testLoadSiteContent1() throws Exception {
        List<String> urls = Arrays.asList("citibank.net");
        Map<String, String> webPageContent = pageDownloader.loadSiteContent(urls);
        assertNotNull(webPageContent);
    }
}
