## Website Searcher
This is Java-based solution built with Command Line Spring Boot and JSoup library.
JSoup used for clean and straightforward WebPage loading and extracting search terms from the HTML content.

Pre Requisite
1. JDK 1.8 - Installation : http://www.oracle.com/technetwork/java/javase/downloads/index.html
2. Maven 3.x  - Installation: https://maven.apache.org/download.cgi
3. Internet access

## Steps to setup
* mvn clean test
* mvn clean install
* java -jar target/websearcher-1.0-SNAPSHOT.jar

The solution download the content from the urls mentioned in urls.txt and build search index. After that, it
prompts for search term to enter on the command line.

## Note
application.properties in resource folder includes configurable application properties such as maxConnections, timeout.


## Limitations
1. The solution has limited validation logic for Input File Urls.txt. For now, it just looks for six entries for each row. In the future, it should be updated to do detailed validations like data type, format, URL validations, etc.
2. The solution has excellent test coverage, but no mocking library got used.
3. The solution faces SocketTimeOutException for a couple of sites. Better connection handling could be used in the future to address it.


# Clarifications
1. What is the primary purpose of this test? Is it mainly focused on candidate's approach to introducing concurrency without thread pooling libraries or how effectively he/she scans a search term or both?
2. Is it ok to use java.util.concurrent.locks/java.util.concurrent.Semaphore in this solution?

## Answer
The primary purpose of the test is to assess your understanding of multi-threading. We have not explicitly prohibited java.util.concurrent.Semaphore, so it should be fine.


## Problem Statement

#Website Searcher
Given a list of URLs in urls.txt: https://s3.amazonaws.com/fieldlens-public/urls.txt, write a program that will fetch each page and determine whether a search term exists on the page (this search can be a  regex - this part isn't too important).

You can make up the search terms. Ignore the additional information in the urls.txt file.

Constraints
* Search is case insensitive
* Should be concurrent.
* But! It shouldn't have more than 20 HTTP requests at any given time.
* The results should be written out to a file results.txt
* Avoid using thread pooling libraries like Executor, ThreadPoolExecutor, Celluloid, or Parallel streams.
* The solution must be written in Kotlin or Java.

Sample urls.txt: https://s3.amazonaws.com/fieldlens-public/urls.txt

The solution must be able to be run from the command line (don't assume JDK is available):
java -jar ./jarFileName.jar

